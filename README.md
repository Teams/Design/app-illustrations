# Illustration Styles in GNOME

## Full Color Style
Illustrations for special application states such as `empty` and `initial` (welcome screens etc). Takes as much attention as possible, humorous, light-hearted.

* Use computer related imagery alongside with positively associated every day life items. Coffee mugs, pencils, plants, pets, animals.
* Avoid depiction of people and faces.
* No textures, mostly flat color fills. Gradients sometimes appropriate for curved surfaces.
* GNOME HIG color palette.
* Geometric shapes rather than organic curvatures.

![Fullcolor Style](overview/styles-fullcolor.png)

### Examples
![GNOME Tour](overview/screenshots/tour-fullcolor.png)
![Boxes Onboarding](overview/screenshots/boxes.png)

## Blueprint Style

Used for documentation, technical details or prototypes. Low fidelity drawings with toned down blue and white fills. Ideal to illustrate future direction without bringing up detail.

* UI elements such as windows, dialogs and icons.
* Less detail. When depicting UIs, avoiding text and simplifying it into blocks/rectangles wherever possible is preferred.
* When needed, isometric perspective can be used.
FIXME: Only works on light backgrounds.

![Fullcolor Style](overview/styles-blueprint.png)

### Examples
![GNOME Tour](overview/screenshots/tour-blueprint.png)
![GNOME Shell Blog](overview/screenshots/gnome-shell-blog.png)